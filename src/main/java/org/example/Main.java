package org.example;

/**
 * The Main class serves as the entry point for the application.
 */
public final class Main {

    // Private constructor to prevent instantiation
    private Main() {
        throw new UnsupportedOperationException("Utility class");
    }

    /**
     * The main method is the entry point of the application.
     *
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }
}
